package = "awesome-server"
version = "dev-1"
source = {
   url = "git+ssh://git@gitlab.com/yowls/awwm-server.git"
}
description = {
   summary = "Server application for awesome wm",
   detailed = "Local server application for graphical config of my awesome window manager rise",
   homepage = "https://gitlab.com/yowls/awwm-server#readme",
   license = "MIT"
}
dependencies = {
   "lua ~> 5.1",
   "moonscript ~> 0.5.0",
   "lapis ~> 1.9.0",
   -- "lapis-console ~> 1.2.0"
}
build = {
   type = "builtin",
   modules = {}
}
