#!/bin/sh

# Install luarocks deps
luarocks build --lua-version=5.1 --only-deps
eval $(luarocks --lua-version=5.1 path)			# export luarock bin path
# FIXME: require install lapis as root for openresty

# Install npm deps
npm install

# Tup build system
tup init
tup generate build.sh && ./build.sh
