lapis = require "lapis"

class App extends lapis.Application
	[index: "/"]: =>
		@title = "Awesome Server"
		-- @page_description = ""

		-- TODO: check if setting file can be read, else send a popup message

		@html ->
			-- IMPORTS
			-- XXX: replace with main-min.css
			link rel: "stylesheet", href: "static/css/main.css"

			-- HOME PAGE
			div class: "container-fluid min-vh-100 d-flex bg-warning p-4", ->

				-- PAGE LEFT SIDE: SIDEBAR
				div class: "navbar navbar-expand flex-column p-4 sidebar-size bg-primary", ->
					-- sidebar header
					a class: "navbar-brand text-light mt-3", ->
						div class: "display-5 font-weight-bold", ->
							-- TODO: add awesome icon here
							h2 "Awesome"
							h4 "Server"

					-- sidebar menu
					ul class: "navbar-nav d-flex flex-column mt-5 w-100", ->
						li class: "nav-item w-100 mb-4", ->
							a class: "nav-link d-flex justify-content-start align-items-center text-light bg-nav-link", ->
								i class: "ps-3 fas fa-igloo"
								div class: "ps-4", "Home"

						li class: "nav-item w-100 mb-4", ->
							a class: "nav-link d-flex justify-content-start align-items-center text-light bg-nav-link", ->
								i class: "ps-3 fas fa-cog"
								div class: "ps-4", "General"

						li class: "nav-item w-100 mb-4", ->
							a class: "nav-link d-flex justify-content-start align-items-center text-light bg-nav-link", ->
								i class: "ps-3 fas fa-ghost"
								div class: "ps-4", "Applications"

						li class: "nav-item w-100 mb-4", ->
							a class: "nav-link d-flex justify-content-start align-items-center text-light bg-nav-link", ->
								i class: "ps-3 fas fa-palette"
								div class: "ps-4", "Theming"

						li class: "nav-item w-100 mb-4", ->
							a class: "nav-link d-flex justify-content-start align-items-center text-light bg-nav-link", ->
								i class: "ps-3 fas fa-user"
								div class: "ps-4", "Profiles"


				-- PAGE RIGHT SIDE
				-- TODO: center content
				-- TODO: work with row and columns here
				div class: "p-4 bg-light", ->
					div class: "justify-content-center", ->
						-- header
						div class: "d-flex justify-content-around", ->
							p "Description"
							p "Status"
							p "Information"

						-- paragraph
						div class: "d-flex align-items-center", ->
							h3 class: "pe-4", "# Welcome to awesome wm config"
							i class: "fas fa-award"

						p "Using lapis framework"
						p "version: #{require "lapis.version"}!"
						p "Lorem ipsum dolor sit cuchufli barquillo bacan jote gamba listeilor po cahuin, luca melon con vino pichanga coscacho ni ahi peinar la muneca chuchada al chancho achoclonar. Chorrocientos pituto ubicatex huevo duro bolsero cachureo el hoyo del queque en cana huevon el ano del loly hacerla corta impeque de miedo quilterry la raja longi necla. Hilo curado rayuela carrete quina guagua lorea piola ni ahi"

	[test: "/test"]: =>
		@title = "testing page"
		home = os.getenv "HOME"
		xdg_config_home = os.getenv "XDG_CONFIG_HOME"

		awesome_dir = ""
		usinga = ""
		if xdg_config_home
			awesome_dir = xdg_config_home .. "/awesome/"
			usinga = "xdg_config_home"
		elseif home
			awesome_dir = home .. "/.config/awesome/"
			usinga = "home"
		else
			awesome_dir = "ERROR"
			usinga = "none"

		setting_file = awesome_dir .. "settings/paths.lua"

		ff = io.open(setting_file, "rb")
		content = ff\read("*all")
		ff\close()

		@html ->
			h2 "+ Outputing env var"
			p "home is: " .. home
			p "xdg config is: " .. xdg_config_home

			h2 "+ Reading file data"
			p "directory: " .. awesome_dir
			p "file: " .. setting_file
			p "using env var: " .. usinga

			h2 "+ Setting file:"
			p content
