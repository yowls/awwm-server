#!/bin/sh

# Restart option -r
if [ $1 = "-r" ]; then
	lapis term
	echo "wait.."
	sleep 3
	lapis server
	exit 0
fi


# Load bin path of lua modules
# TODO: change to lapis
if [ ! $(command -v moonc) ]; then
	echo "-- Added luarocks libs to path"
	eval $(luarocks --lua-version=5.1 path)
fi

# Start HTTP Server
lapis server

# Start Tup monitor mode
tup monitor -a
