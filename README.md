## 🖥️ Awesome Server Mode

( git ignore add templates from:) <br>
( - lapis new --git --tup       ) <br>
( - luarocks init               ) <br>
( - npm init                    ) <br>
( - lua && moonscript           ) <br>
( - javascript && coffescript   ) <br>
( - css && scss                 )

<br>

###### Modified dirs for openresty
/usr/local/lib/
/usr/local/share/

<br>

### Requirements
+ *Languages*:..................lua 5.1
+ *Package Manager*:............[luarocks](https://luarocks.org/) + [npm](https://www.npmjs.com/)
+ *Web Framework*:..............[lapis](https://leafo.net/lapis/)
  + install: sudo luarocks install --lua-version=5.1 lapis
+ *Config*:.....................[my awesome config](https://gitlab.com/yowls/awwm/)
+ *HTTP Server*:................[openresty](https://openresty.org/en/)
+ *Build system*:...............[tup](http://gittup.org/tup/)

<br>

### Pending

+ *Framework Actions*:
  + install compiler: ....... npm install coffeescript
  + install lib: ............ npm install animejs

<br>

### Installation
git clone https://gitlab.com/yowls/awwm-server  <br>
./setup.sh                                      <br>
lapis server


  [1]: https://github.com/luarocks/luarocks/wiki/Installation-instructions-for-Unix
  [2]: https://nodejs.org/en/download/
